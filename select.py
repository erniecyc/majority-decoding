

['oh do you wanna come over to my place ?', 'oh do you wanna come over to my place tonight ?']


sents = [
    "oh do you wanna come over to my place ?",
    "so do you wanna come over to my place right ?",
    "oh do you wanna come over to my place tonight ?",
    "oh so do you wanna come over to my",
    "oh so do you wanna come over to my place ?",
    "oh so do you wanna ?",
    "do you come home ?",
    "are you coming home ?",
    "come home, right",
    "this is going to be home come home",
]


def collect_stats(sents):
    res, N, max_dict = {}, range(2, 4), {}
    for sent in sents:
        sentence = sent.split()
        for n in N:
            ngrams = [(i, n, ' '.join(sentence[i: i + n])) for i in range(len(sentence) - n + 1)]
            for ngram in ngrams:
                res[ngram] = res.get(ngram, 0) + 1
    for (i, n, ngram) in res.keys():
        max_dict[(i, n)] = max_dict.get((i, n), []) + [res[(i, n, ngram)]]
    for (i, n) in max_dict.keys():
        max_dict[(i, n)] = max(max_dict[(i, n)])
    return res, max_dict

 
def majority_decode(remain_sents, stats, max_dict, target_n):
    max_len = max([len(sent.split()) for sent in sents])
    for step in range(max_len):
        # eliminate sents for each step 
        for (i, n, ngram) in stats:            
            if step == i and n == target_n:
                new_remain_sents = remain_sents
                for sent in remain_sents:
                    if stats[(i, n, ngram)] < max_dict[(i, n)] and ' '.join(sent[i: i+n]) == ngram:
                        new_remain_sents.remove(sent)
                    if len(new_remain_sents) == 1:
                        return new_remain_sents
                remain_sents = new_remain_sents
    return remain_sents


stats, max_dict = collect_stats(sents)
# print(stats)

sents = majority_decode(
    remain_sents=[sent.split() for sent in sents], 
    stats=stats, 
    max_dict=max_dict, 
    target_n=3,
)
sents = [' '.join(sent) for sent in sents]
print(sents)


